package com.example.customalarmclock;
import java.util.Calendar;
import java.util.Date;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;


@SuppressLint("ValidFragment")
public class TimePickerFragment extends DialogFragment implements 
	TimePickerDialog.OnTimeSetListener {
	Context main;
	
	public TimePickerFragment(Context mainActivity) {
		main = mainActivity;
	}

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

	@Override
	public void onTimeSet(TimePicker arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		Alarm alarm = new Alarm();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, arg1);
		cal.set(Calendar.MINUTE, arg2);
		cal.set(Calendar.SECOND, 0);
	    alarm.setAlarm(main, cal.getTimeInMillis());
	}

}
