package com.example.customalarmclock;

import com.google.android.youtube.player.YouTubeIntents;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

public class VideoPlayer extends View {
	Context c = null;

	public VideoPlayer(Context context) {
		super(context);
		c = context;
		
	}
	
	public void playVideo() {
		WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.RIGHT | Gravity.TOP;
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        wm.addView(this, params);
		Intent playVideo = YouTubeIntents.createPlayVideoIntentWithOptions(c, "zhXPA35DUUw", true, true);
        playVideo.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(playVideo);
	}
	

}
